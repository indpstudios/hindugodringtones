package com.indpstudios.godringtones.ProjectUtils;

import android.view.View;

/**
 * Created by Developer on 3/6/2018.
 */

public interface ClickListener {
    void onClick(View view, int position);
}
