package com.indpstudios.godringtones.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indpstudios.godringtones.ModelClass.FMSubDataMain;
import com.indpstudios.godringtones.R;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    Context context;
    private ArrayList<FMSubDataMain> fmSubDataMains = null;
    LayoutInflater inflater;

    public ViewPagerAdapter(Context activity1, ArrayList<FMSubDataMain> menu) {
        this.context = activity1;
        this.fmSubDataMains = menu;
    }

    @Override
    public int getCount() {
        return fmSubDataMains.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.viewpager_item, container, false);
        final TextView Id_HeaderName = (TextView) view.findViewById(R.id.Id_HeaderName);
        container.addView(view);
        Id_HeaderName.setText(fmSubDataMains.get(position).getHeaderName());
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewGroup) container).removeView((View) object);
    }
}
