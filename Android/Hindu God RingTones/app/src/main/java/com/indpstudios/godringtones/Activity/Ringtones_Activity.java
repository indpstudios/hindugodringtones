package com.indpstudios.godringtones.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.indpstudios.godringtones.Adapters.PlayMusicAdapter;
import com.indpstudios.godringtones.ModelClass.FMSubDataMain;
import com.indpstudios.godringtones.ProjectUtils.PlayMusicListener;
import com.indpstudios.godringtones.ProjectUtils.ProjectMethods;
import com.indpstudios.godringtones.ProjectUtils.StopPlayingListener;
import com.indpstudios.godringtones.R;

import java.util.ArrayList;


public class Ringtones_Activity extends AppCompatActivity implements PlayMusicListener, StopPlayingListener {
    private RecyclerView Id_MainRecyclerView;
    private ArrayList<FMSubDataMain> mainDetailsArrayList;
    private PlayMusicAdapter dashBoardAdapter;
    private int[] RawFilesArray = {
            R.raw.a1,
            R.raw.a2,
            R.raw.a3,
            R.raw.a4,
            R.raw.a5,
            R.raw.a6,
            R.raw.a7,
            R.raw.a8,
            R.raw.a9,
            R.raw.a10,
            R.raw.a11,
            R.raw.a12,
            R.raw.a13,
            R.raw.a14,
            R.raw.a15,
            R.raw.a16,
            R.raw.a17,
            R.raw.a18,
            R.raw.a19,
            R.raw.a20,
            R.raw.a21,
            R.raw.a22,
            R.raw.a23,
            R.raw.a24,
            R.raw.a25,
            R.raw.a26,
            R.raw.a27,
            R.raw.a28,
            R.raw.a29,
            R.raw.a30,
            R.raw.a31,
            R.raw.a32,
            R.raw.a33,
            R.raw.a34,
            R.raw.a35,
            R.raw.a36,
            R.raw.a37,
            R.raw.a38,
            R.raw.a39,
            R.raw.a40,
            R.raw.a41,
            R.raw.a42,
            R.raw.a43,
            R.raw.a44,
            R.raw.a45,
            R.raw.a46,
            R.raw.a47,
            R.raw.a48,
            R.raw.a49,
            R.raw.a50,
            R.raw.a51,
            R.raw.a52,
            R.raw.a53,
            R.raw.a54,
            R.raw.a55,
            R.raw.a56,
            R.raw.a57,
            R.raw.a58,
            R.raw.a59,
            R.raw.a60,
            R.raw.a61,
            R.raw.a62,
            R.raw.a63,
            R.raw.a64,
            R.raw.a65,
            R.raw.a66,
            R.raw.a67,
            R.raw.a68,
            R.raw.a69,
            R.raw.a70,
            R.raw.a71,
            R.raw.a72,
            R.raw.a73,
            R.raw.a74,
            R.raw.a75,
            R.raw.a76,
            R.raw.a77,
            R.raw.a78,
            R.raw.a79,
            R.raw.a80,
            R.raw.a81,
            R.raw.a82,
            R.raw.a83,
            R.raw.a84,
            R.raw.a85,
            R.raw.a86,
            R.raw.a87,
            R.raw.a88,
            R.raw.a89,
            R.raw.a90,
            R.raw.a91,
            R.raw.a92,
            R.raw.a93,
            R.raw.a94,
            R.raw.a95,
            R.raw.a96,
            R.raw.a97,
            R.raw.a98,
            R.raw.a99,
            R.raw.a100,
            R.raw.a101,
            R.raw.a102,
            R.raw.a103,
            R.raw.a104,
            R.raw.a105,
            R.raw.a106,
            R.raw.a107,
            R.raw.a108,
            R.raw.a109,
            R.raw.a110,
            R.raw.a111,
            R.raw.a112,
            R.raw.a113,
            R.raw.a114,
            R.raw.a115,
            R.raw.a116,
            R.raw.a117,
            R.raw.a118,
            R.raw.a119,
            R.raw.a120,
            R.raw.a121,
            R.raw.a122,
            R.raw.a123,
            R.raw.a124,
            R.raw.a125,
            R.raw.a126,
            R.raw.a127,
            R.raw.a128,
            R.raw.a129,
            R.raw.a130,
            R.raw.a131,
            R.raw.a132,
            R.raw.a133,
            R.raw.a134,
            R.raw.a135,
            R.raw.a136,
            R.raw.a137,
            R.raw.a138,
            R.raw.a139,
            R.raw.a140,
            R.raw.a141,
            R.raw.a142,
            R.raw.a143,
            R.raw.a144,
            R.raw.a145,
            R.raw.a146,
            R.raw.a147,
            R.raw.a148,
            R.raw.a149,
            R.raw.a150,
            R.raw.a151,
            R.raw.a152,
            R.raw.a153,
            R.raw.a154,
            R.raw.a155,
            R.raw.a156,
            R.raw.a157,
            R.raw.a158,
            R.raw.a159,
            R.raw.a160,
            R.raw.a161,
            R.raw.a162,
            R.raw.a163,
            R.raw.a164,
            R.raw.a165,
            R.raw.a166,
            R.raw.a167,
            R.raw.a168,
            R.raw.a169,
            R.raw.a170,
            R.raw.a171,
            R.raw.a172,
            R.raw.a173,
            R.raw.a174,
            R.raw.a175,
            R.raw.a176,
            R.raw.a177,
            R.raw.a178,
            R.raw.a179,
            R.raw.a180,
            R.raw.a181,
            R.raw.a182,
            R.raw.a183,
            R.raw.a184,
            R.raw.a185,
            R.raw.a186,
            R.raw.a187,
            R.raw.a188,
            R.raw.a189,
            R.raw.a190,
            R.raw.a191,
            R.raw.a192,
            R.raw.a193,
            R.raw.a194,
            R.raw.a195,
            R.raw.a196,
            R.raw.a197,
            R.raw.a198,
            R.raw.a199,
            R.raw.a200,
            R.raw.a201,
            R.raw.a202,
            R.raw.a203,
            R.raw.a204,
            R.raw.a205,
            R.raw.a206,
            R.raw.a207,
            R.raw.a208,
            R.raw.a209,
            R.raw.a210,
            R.raw.a211,
            R.raw.a212,
            R.raw.a213,
            R.raw.a214,
            R.raw.a215,
            R.raw.a216,
            R.raw.a217,
            R.raw.a218,
            R.raw.a219,
            R.raw.a220,
            R.raw.a221



    };
    private String[] MusicNamesArray = {
            "a1",
            "a2",
            "a3",
            "a4",
            "a5",
            "a6",
            "a7",
            "a8",
            "a9",
            "a10",
            "a11",
            "a12",
            "a13",
            "a14",
            "a15",
            "a16",
            "a17",
            "a18",
            "a19",
            "a20",
            "a21",
            "a22",
            "a23",
            "a24",
            "a25",
            "a26",
            "a27",
            "a28",
            "a29",
            "a30",
            "a31",
            "a32",
            "a33",
            "a34",
            "a35",
            "a36",
            "a37",
            "a38",
            "a39",
            "a40",
            "a41",
            "a42",
            "a43",
            "a44",
            "a45",
            "a46",
            "a47",
            "a48",
            "a49",
            "a50",
            "a51",
            "a52",
            "a53",
            "a54",
            "a55",
            "a56",
            "a57",
            "a58",
            "a59",
            "a60",
            "a61",
            "a62",
            "a63",
            "a64",
            "a65",
            "a66",
            "a67",
            "a68",
            "a69",
            "a70",
            "a71",
            "a72",
            "a73",
            "a74",
            "a75",
            "a76",
            "a77",
            "a78",
            "a79",
            "a80",
            "a81",
            "a82",
            "a83",
            "a84",
            "a85",
            "a86",
            "a87",
            "a88",
            "a89",
            "a90",
            "a91",
            "a92",
            "a93",
            "a94",
            "a95",
            "a96",
            "a97",
            "a98",
            "a99",
            "a100",
            "a101",
            "a102",
            "a103",
            "a104",
            "a105",
            "a106",
            "a107",
            "a108",
            "a109",
            "a110",
            "a111",
            "a112",
            "a113",
            "a114",
            "a115",
            "a116",
            "a117",
            "a118",
            "a119",
            "a120",
            "a121",
            "a122",
            "a123",
            "a124",
            "a125",
            "a126",
            "a127",
            "a128",
            "a129",
            "a130",
            "a131",
            "a132",
            "a133",
            "a134",
            "a135",
            "a136",
            "a137",
            "a138",
            "a139",
            "a140",
            "a141",
            "a142",
            "a143",
            "a144",
            "a145",
            "a146",
            "a147",
            "a148",
            "a149",
            "a150",
            "a151",
            "a152",
            "a153",
            "a154",
            "a155",
            "a156",
            "a157",
            "a158",
            "a159",
            "a160",
            "a161",
            "a162",
            "a163",
            "a164",
            "a165",
            "a166",
            "a167",
            "a168",
            "a169",
            "a170",
            "a171",
            "a172",
            "a173",
            "a174",
            "a175",
            "a176",
            "a177",
            "a178",
            "a179",
            "a180",
            "a181",
            "a182",
            "a183",
            "a184",
            "a185",
            "a186",
            "a187",
            "a188",
            "a189",
            "a190",
            "a191",
            "a192",
            "a193",
            "a194",
            "a195",
            "a196",
            "a197",
            "a198",
            "a199",
            "a200",
            "a201",
            "a202",
            "a203",
            "a204",
            "a205",
            "a206",
            "a207",
            "a208",
            "a209",
            "a210",
            "a211",
            "a212",
            "a213",
            "a214",
            "a215",
            "a216",
            "a217",
            "a218",
            "a219",
            "a220",
            "a221",




    };
    private String[] HeaderNamesArray = {
            "Ring Tone1",
            "Ring Tone2",
            "Ring Tone3",
            "Ring Tone4",
            "Ring Tone5",
            "Ring Tone6",
            "Ring Tone7",
            "Ring Tone8",
            "Ring Tone9",
            "Ring Tone10",
            "Ring Tone11",
            "Ring Tone12",
            "Ring Tone13",
            "Ring Tone14",
            "Ring Tone15",
            "Ring Tone16",
            "Ring Tone17",
            "Ring Tone18",
            "Ring Tone19",
            "Ring Tone20",
            "Ring Tone21",
            "Ring Tone22",
            "Ring Tone23",
            "Ring Tone24",
            "Ring Tone25",
            "Ring Tone26",
            "Ring Tone27",
            "Ring Tone28",
            "Ring Tone29",
            "Ring Tone30",
            "Ring Tone31",
            "Ring Tone32",
            "Ring Tone33",
            "Ring Tone34",
            "Ring Tone35",
            "Ring Tone36",
            "Ring Tone37",
            "Ring Tone38",
            "Ring Tone39",
            "Ring Tone40",
            "Ring Tone41",
            "Ring Tone42",
            "Ring Tone43",
            "Ring Tone44",
            "Ring Tone45",
            "Ring Tone46",
            "Ring Tone47",
            "Ring Tone48",
            "Ring Tone49",
            "Ring Tone50",
            "Ring Tone51",
            "Ring Tone52",
            "Ring Tone53",
            "Ring Tone54",
            "Ring Tone55",
            "Ring Tone56",
            "Ring Tone57",
            "Ring Tone58",
            "Ring Tone59",
            "Ring Tone60",
            "Ring Tone61",
            "Ring Tone62",
            "Ring Tone63",
            "Ring Tone64",
            "Ring Tone65",
            "Ring Tone66",
            "Ring Tone67",
            "Ring Tone68",
            "Ring Tone69",
            "Ring Tone70",
            "Ring Tone71",
            "Ring Tone72",
            "Ring Tone73",
            "Ring Tone74",
            "Ring Tone75",
            "Ring Tone76",
            "Ring Tone77",
            "Ring Tone78",
            "Ring Tone79",
            "Ring Tone80",
            "Ring Tone81",
            "Ring Tone82",
            "Ring Tone83",
            "Ring Tone84",
            "Ring Tone85",
            "Ring Tone86",
            "Ring Tone87",
            "Ring Tone88",
            "Ring Tone89",
            "Ring Tone90",
            "Ring Tone91",
            "Ring Tone92",
            "Ring Tone93",
            "Ring Tone94",
            "Ring Tone95",
            "Ring Tone96",
            "Ring Tone97",
            "Ring Tone98",
            "Ring Tone99",
            "Ring Tone100",
            "Ring Tone101",
            "Ring Tone102",
            "Ring Tone103",
            "Ring Tone104",
            "Ring Tone105",
            "Ring Tone106",
            "Ring Tone107",
            "Ring Tone108",
            "Ring Tone109",
            "Ring Tone110",
            "Ring Tone111",
            "Ring Tone112",
            "Ring Tone113",
            "Ring Tone114",
            "Ring Tone115",
            "Ring Tone116",
            "Ring Tone117",
            "Ring Tone118",
            "Ring Tone119",
            "Ring Tone120",
            "Ring Tone121",
            "Ring Tone122",
            "Ring Tone123",
            "Ring Tone124",
            "Ring Tone125",
            "Ring Tone126",
            "Ring Tone127",
            "Ring Tone128",
            "Ring Tone129",
            "Ring Tone130",
            "Ring Tone131",
            "Ring Tone132",
            "Ring Tone133",
            "Ring Tone134",
            "Ring Tone135",
            "Ring Tone136",
            "Ring Tone137",
            "Ring Tone138",
            "Ring Tone139",
            "Ring Tone140",
            "Ring Tone141",
            "Ring Tone142",
            "Ring Tone143",
            "Ring Tone144",
            "Ring Tone145",
            "Ring Tone146",
            "Ring Tone147",
            "Ring Tone148",
            "Ring Tone149",
            "Ring Tone150",
            "Ring Tone151",
            "Ring Tone152",
            "Ring Tone153",
            "Ring Tone154",
            "Ring Tone155",
            "Ring Tone156",
            "Ring Tone157",
            "Ring Tone158",
            "Ring Tone159",
            "Ring Tone160",
            "Ring Tone161",
            "Ring Tone162",
            "Ring Tone163",
            "Ring Tone164",
            "Ring Tone165",
            "Ring Tone166",
            "Ring Tone167",
            "Ring Tone168",
            "Ring Tone169",
            "Ring Tone170",
            "Ring Tone171",
            "Ring Tone172",
            "Ring Tone173",
            "Ring Tone174",
            "Ring Tone175",
            "Ring Tone176",
            "Ring Tone177",
            "Ring Tone178",
            "Ring Tone179",
            "Ring Tone180",
            "Ring Tone181",
            "Ring Tone182",
            "Ring Tone183",
            "Ring Tone184",
            "Ring Tone185",
            "Ring Tone186",
            "Ring Tone187",
            "Ring Tone188",
            "Ring Tone189",
            "Ring Tone190",
            "Ring Tone191",
            "Ring Tone192",
            "Ring Tone193",
            "Ring Tone194",
            "Ring Tone195",
            "Ring Tone196",
            "Ring Tone197",
            "Ring Tone198",
            "Ring Tone199",
            "Ring Tone200",
            "Ring Tone201",
            "Ring Tone202",
            "Ring Tone203",
            "Ring Tone204",
            "Ring Tone205",
            "Ring Tone206",
            "Ring Tone207",
            "Ring Tone208",
            "Ring Tone209",
            "Ring Tone210",
            "Ring Tone211",
            "Ring Tone212",
            "Ring Tone213",
            "Ring Tone214",
            "Ring Tone215",
            "Ring Tone216",
            "Ring Tone217",
            "Ring Tone218",
            "Ring Tone219",
            "Ring Tone220",
            "Ring Tone221"



    };
    private PlayMusicListener playMusicListener;
    private StopPlayingListener stopPlayingListener;
    private static final int REQUEST_WRITE_PERMISSION = 1001;
    private int ItemCurrentposition = 0;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_all_stations);
            playMusicListener = this;
            stopPlayingListener = this;
            mainDetailsArrayList = new ArrayList<>();
            Id_MainRecyclerView = (RecyclerView) findViewById(R.id.Id_MainRecyclerView);
            checkSystemWritePermission();
            checkPermission();
            LoadGoogleAdds();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            if (ProjectMethods.mediaPlayer != null) {

            } else {
                ProjectMethods.mediaPlayer = new MediaPlayer();
            }
            FMRadioListData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadGoogleAdds() {
        MobileAds.initialize(this, getString(R.string.AppId));
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public void PlayMusicMethod() {
        try {
            StopMusic();
            StartMusic();
            dashBoardAdapter.setSelectedItem(ItemCurrentposition);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void StopMusic() {
        try {
            if (ProjectMethods.mediaPlayer != null) {
                ProjectMethods.mediaPlayer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void StartMusic() {
        try {
            String uriPath = "android.resource://" + getPackageName() + "/raw/" + mainDetailsArrayList.get(ItemCurrentposition).getMusicUrl();
            Uri uri = Uri.parse(uriPath);
            ProjectMethods.mediaPlayer.reset();
            //  final AssetFileDescriptor sound = getResources().openRawResourceFd(animalsound);
            ProjectMethods.mediaPlayer.setDataSource(Ringtones_Activity.this, uri);
            ProjectMethods.mediaPlayer.setLooping(true);
            ProjectMethods.mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            ProjectMethods.mediaPlayer.prepareAsync();
            ProjectMethods.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            ProjectMethods.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });
            //   dashBoardAdapter.setSelectedItem(ItemCurrentposition);
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong..!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void OpenSystemSettings() {
        try {
            new AlertDialog.Builder(Ringtones_Activity.this)
                    .setTitle("Permission Required")
                    .setMessage("Access to system permission is required to set ringtone.")
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    openAndroidPermissionsMenu();
                                    dialog.cancel();
                                }
                            })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkSystemWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(Ringtones_Activity.this))
                return true;
            else
                OpenSystemSettings();
        }
        return false;
    }

    private void openAndroidPermissionsMenu() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
            ) {
                String[] permissions = new String[]{
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_SETTINGS,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                };
                requestPermissions(permissions, 100);
            }
        }
    }

    private void FMRadioListData() {
        mainDetailsArrayList = new ArrayList<>();
        try {
            for (int i = 0; i < RawFilesArray.length; i++) {
                FMSubDataMain obj_dish = new FMSubDataMain();
                obj_dish.setHeaderName(HeaderNamesArray[i]);
                obj_dish.setRawFile(RawFilesArray[i]);
                obj_dish.setMusicUrl(MusicNamesArray[i]);
                mainDetailsArrayList.add(obj_dish);
            }
            if (mainDetailsArrayList.size() > 0) {
                dashBoardAdapter = new PlayMusicAdapter(Ringtones_Activity.this, mainDetailsArrayList, playMusicListener, stopPlayingListener);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Ringtones_Activity.this);
                Id_MainRecyclerView.setLayoutManager(mLayoutManager);
                Id_MainRecyclerView.setHasFixedSize(true);
                int resId = R.anim.layout_animation_down_to_up;
                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(Ringtones_Activity.this, resId);
                Id_MainRecyclerView.setLayoutAnimation(animation);
                Id_MainRecyclerView.setItemAnimator(new DefaultItemAnimator());
                Id_MainRecyclerView.setAdapter(dashBoardAdapter);
            } else {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (ProjectMethods.mediaPlayer != null) {
                    ProjectMethods.mediaPlayer.stop();
                }
                finish();
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            int i = 0;
            for (int grant : grantResults) {
                if (grant == PackageManager.PERMISSION_DENIED) {
                    i++;
                }
            }
            if (i > 0) {
                showDialog(0);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (ProjectMethods.mediaPlayer != null) {
            ProjectMethods.mediaPlayer.stop();
        }
        finish();
    }

    @Override
    public void StartPlaying(int position) {
        ItemCurrentposition = position;
        PlayMusicMethod();
    }

    @Override
    public void StopPlaying(int position) {
        ItemCurrentposition = position;
        StopMusic();
    }
}
