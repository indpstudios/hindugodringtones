package com.indpstudios.godringtones.ProjectUtils;

/**
 * Created by Developer on 3/6/2018.
 */

public interface StopPlayingListener {
    void StopPlaying(int position);
}
