package com.indpstudios.godringtones.ModelClass;

import java.io.Serializable;


public class FMSubDataMain implements Serializable {
    private String HeaderName = "";
    private int RawFile;
    private String MusicUrl = "";

    public int getRawFile() {
        return RawFile;
    }

    public void setRawFile(int rawFile) {
        RawFile = rawFile;
    }

    public String getMusicUrl() {
        return MusicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        MusicUrl = musicUrl;
    }

    public String getHeaderName() {
        return HeaderName;
    }

    public void setHeaderName(String headerName) {
        HeaderName = headerName;
    }
}
