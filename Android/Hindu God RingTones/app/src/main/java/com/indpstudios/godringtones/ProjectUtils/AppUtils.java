package com.indpstudios.godringtones.ProjectUtils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppUtils {
    private static SharedPreferences sharedPreferences;
    private static int year;
    private static String month;

    private static String date;
    public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
    private static void initializeSharedPreference() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication.getGlobalContext());
    }

    public static void updateSharedPreference(String key, String value) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void updateSharedPreference(String key, int value) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void updateSharedPreference(String key, boolean value) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String getSharedPreferenceValue(String key) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        if (key != null) {
            return sharedPreferences.getString(key, null);
        } else {
            return null;
        }
    }

    public static String getSharedPreferenceFoRnotificationValue(String key) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        if (key != null) {
            return sharedPreferences.getString(key, "");
        } else {
            return "";
        }
    }


    public static boolean getSharedPreferenceValueBoolean(String key) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        if (key != null) {
            return sharedPreferences.getBoolean(key, false);
        } else {
            return false;
        }
    }

    public static int getSharedPreferenceValueInt(String key) {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }

        if (key != null) {
            return sharedPreferences.getInt(key, 0);
        } else {
            return 0;
        }
    }
    public static void clearSharedPreference() {
        if (sharedPreferences == null) {
            initializeSharedPreference();
        }
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
        }
    }
}
